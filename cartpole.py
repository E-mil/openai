import gym
import time
import random
import json
import ast
from counter import Counter

# Q-LEARNING
# Q(s_t, a_t)' = (1 - alpha) * Q(s_t, a_t) + alpha * (r + gamma * max_a (Q(s_t+1, a)))
# alpha is the learning rate and gamma is the discount factor

training = True

class queue_element():

    def __init__(self, entry):
        self.entry = entry
        self.next_element = None

    def set_next_element(self, element):
        self.next_element = element

    def get_next_element(self):
        return self.next_element

class queue():

    def __init__(self):
        self.first = None
        self.last = None
        self.nbr_entries = 0

    def append(self, entry):
        self.nbr_entries += 1
        if self.first == None:
            self.first = queue_element(entry)
            self.last = self.first
        else:
            element = queue_element(entry)
            self.last.set_next_element(element)
            self.last = element

    def pop(self):
        if self.first != None:
            self.nbr_entries -= 1
            self.first = self.first.get_next_element()

    def get_entries_as_tuple(self):
        entries = []
        element = self.first
        while element != None:
            entries.append(element.entry)
            element = element.get_next_element()
        return tuple(entries)

class q_agent():

    def __init__(self):
        self.granularity = 50
        self.alpha = 0.25
        self.gamma = 0.5

        self.env = gym.make('CartPole-v0')
        self.env.reset()
        self.env.render()
        self.q_values = Counter()

        self.last_actions = queue()
        self.last_angles = queue()

    def train(self, nbr_episodes):
        max_epsilon = 0.8
        for i in range(nbr_episodes):
            epsilon = max_epsilon - (0.6 * i) / nbr_episodes
            observation = self.env.reset()
            state = self.get_state(observation, 0)
            self.last_actions = queue()
            self.last_angles = queue()
            countDown = 1
            if i % 50 == 0:
                print("Training..... Episode {} of {}".format(i, nbr_episodes))
            for j in range(500):
                action = self.get_action(state, epsilon)
                observation, reward, done, _ = self.env.step(action)
                last_state = state
                state = self.get_state(observation, action)
                if done and j < 199:
                    reward = -1
                else:
                    reward = 0
                self.update_q_value(last_state, action, reward, state)
                if done:
                    countDown -= 1
                    if countDown == 0:
                        break

    def set_q_values(self, dictionary):
        self.q_values = Counter(dictionary)

    def test(self):
        for _ in range(15):
            observation = self.env.reset()
            action = 0
            self.last_actions = queue()
            self.last_angles = queue()
            for i in range(1000):
                self.env.render()
                state = self.get_state(observation, action)
                action = self.get_action(state, 0)
                #####
                #time.sleep(2)
                #print(str(state) + " : " + str(action))
                #####
                observation, reward, done, _ = self.env.step(action)
                if done:
                    print(i)
                    break

    def random_test(self):
        for _ in range(15):
            self.env.reset()
            for i in range(1000):
                self.env.render()
                action = self.env.action_space.sample()
                _, reward, done, _ = self.env.step(action)
                if done:
                    print(i)
                    break

    def get_action(self, state, epsilon):
        if random.random() < epsilon:
            return self.env.action_space.sample()
        best_action = None
        best_value = -10000
        #sum_of_values = 0
        for action in range(self.env.action_space.n):
            value = self.q_values[(state, action)]
        #    sum_of_values += value
            if value > best_value:
                best_value = value
                best_action = action
        #if sum_of_values != 0:
        #    ratio = best_value / sum_of_values
        #    if ratio > epsilon:
        #        return best_action
        #return self.env.action_space.sample()
        return best_action

    def update_q_value(self, last_state, action, reward, state):
        best_future_reward = -10000
        for i in [0, 1]:
            future_reward = self.q_values[(state, i)]
            if future_reward > best_future_reward:
                best_future_reward = future_reward
        self.q_values[(last_state, action)] = (1 - self.alpha) \
            * self.q_values[(last_state, action)] \
            + self.alpha * (reward + self.gamma * best_future_reward)

    def get_state(self, observation, action):
        if self.last_actions.nbr_entries > 5:
            self.last_actions.pop()
        if self.last_angles.nbr_entries > 2:
            self.last_angles.pop()
        self.last_actions.append(action)
        self.last_angles.append(int(observation[2] * self.granularity))
        return (int(observation[0] * 0.7), self.last_angles.get_entries_as_tuple() \
            , self.last_actions.get_entries_as_tuple())

def dump_dict_to_file(dictionary, filename):
    with open(filename, "w") as file:
        temp = {}
        for key in dictionary:
            temp[str(key)] = dictionary[key]
        json.dump(temp, file)

def get_dict_from_file(filename):
    dictionary = {}
    with open(filename, "r") as file:
        temp = json.load(file)
        for key in temp:
            dictionary[ast.literal_eval(key)] = temp[key]
    return dictionary

agent = q_agent()
#agent_brain = get_dict_from_file("agent_brain.json")
#agent.set_q_values(agent_brain)
agent.train(40000)
training = False
print("random_test")
#agent.random_test()
print("agent_test")
agent.test()
dump_dict_to_file(agent.q_values, "agent_brain.json")